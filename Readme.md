1. Make sure you have sox download on your PC(for linux)
2. cd voice_recorder
3. run chmod 777 rec.sh
       chmod 777 test.sh
   to make all .sh files readable.
4. run ./rec.sh
   (1)After your run this script, one sentence from corpus.txt will show in terminal, read that and your voice will be recorded automatically.
   (2)Press Ctrl+C, then next sentence will show up and repeat (1) and (2) until all 16 sentences have been recorded.
   (3) After your finished this step, your folder should have 16 .wav files named from arctic_0001 to arctic_0016  
5. run ./test.sh
   to check out every .wav file, all the files will be played automatically
6. If you are not using a computer with linux system or can't use sox, you can record your voice with other software. Make sure that every .wav file is recorded at a rate of 16 kHz.
